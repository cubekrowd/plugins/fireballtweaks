package net.cubekrowd.fireballtweaks;

import javassist.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.regex.Pattern;

public class Agent {

    /*
    Sets the explosion durability of a moving block to the durability of the block being pushed. When an explosion
    occurs, the Explosion class determines the explosive durability of each block by delegating to the instance of
    ExplosionDamageCalculator in a field. The method of ExplosionDamageCalculator used to determine the explosive
    durability of a block takes parameters of type Explosion, IBlockAccess, BlockPosition, IBlockData, Fluid and
    returns an Optional<Float>. This injection is added to the top of that method. The injection works by performing
    the exact same calculation as would normally occur, except substituting the durability of the carried block instead
    of the durability of the moving piston itself.

    The injection target is specified to be any method which returns an Optional (of any contained type) in the
    ExplosionDamageCalculator class.
     */
    private static final String EXPLOSION_DAMAGE_CALCULATOR_INJECTION =
            "{\n" +
                    // Get the block being exploded.
                    "$NMS.Block block = $4.getBlock();\n" +
                    // If it is a moving piston,
                    "if (block instanceof $NMS.BlockPistonMoving) {\n" +
                    // then fetch its tile entity data.
                    "    $NMS.TileEntity tileEntity = $1.source.getWorld().getTileEntity($3);\n" +
                    // If its tile entity data is moving piston tile entity data (which it should be),
                    "    if (tileEntity instanceof $NMS.TileEntityPiston) {\n" +
                    "        $NMS.TileEntityPiston tileEntityPiston = ($NMS.TileEntityPiston) tileEntity;\n" +
                    // then recompute the durability based on the durability of the block being pushed.
                    "        float durability = Math.max($5.i(), tileEntityPiston.k().getBlock().getDurability());\n" +
                    // Then return the new durability instead of continuing with the rest of the method.
                    "        return java.util.Optional.of(Float.valueOf(durability));\n" +
                    "    }\n" +
                    "}\n" +
                    "}";

    /*
    Sets the behaviour of fireballs when they are pushed. When an entity is pushed, the Entity#getPushReaction() method
    determine the appropriate response (via an EnumPistonReaction). This injection overrides that method in the
    EntityFireball class and causes it to return EnumPistonReaction.IGNORE instead of using the default implementation
    which returns EnumPistonReaction.NORMAL.

    The injection target is specified to be the method named getPushReaction in the class EntityFireball. Note that if
    the Entity#getPushReaction() method were removed, a new method would be defined instead of a super method being
    overridden.
    */
    private static final String ENTITY_FIREBALL_INJECTION =
            // Declare the method so it is overridden
            "public $NMS.EnumPistonReaction getPushReaction() {\n" +
                    // Return the IGNORE response to disable fireball bouncing
                    "return $NMS.EnumPistonReaction.IGNORE;\n" +
                    "}";

    // Matches the ExplosionDamageCalculator class
    private static final Pattern EXPLOSION_DAMAGE_CALCULATOR_REGEX =
            Pattern.compile("net/minecraft/server/v[_a-zA-Z0-9]+/ExplosionDamageCalculator");

    // Matches the EntityFireball class
    private static final Pattern ENTITY_FIREBALL_REGEX =
            Pattern.compile("net/minecraft/server/v[_a-zA-Z0-9]+/EntityFireball");

    private static ClassPool classPool;
    private static boolean hasInjectedIntoExplosionDamageCalculator = false;
    private static boolean hasInjectedIntoEntityFireball = false;

    // If you are unfamiliar with Java Agents, this function will be called before the main method and modifies the
    // JVM's class loading
    public static void premain(String args, Instrumentation instrumentation) {
        // The class pool tracks classes as they are loaded so injected code can reference other classes
        classPool = new ClassPool();
        classPool.appendSystemPath();

        // Requests that the JVM filter all classes through the following as they are loaded
        instrumentation.addTransformer(new ClassFileTransformer() {

            @Override
            public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) {
                // Don't bother with anything if we've processed everything and cleared the class pool
               if (classPool == null) {
                   return null;
               }

                try {
                    // Register every class with the class pool
                    classPool.makeClass(new ByteArrayInputStream(classfileBuffer));

                    // Replace the bytecode of net.minecraft.server.<version>.ExplosionDamageCalculator with a
                    // modified version
                    if (EXPLOSION_DAMAGE_CALCULATOR_REGEX.matcher(className).matches()) {
                        byte[] newClass = redefineExplosionDamageCalculator(className);
                        hasInjectedIntoExplosionDamageCalculator = true;
                        clearClassPoolIfNeeded();
                        return newClass;
                    }

                    // Replace the bytecode of net.minecraft.server.<version>.EntityFireball with a modified version
                    if (ENTITY_FIREBALL_REGEX.matcher(className).matches()) {
                        byte[] newClass = redefineFireball(className);
                        hasInjectedIntoEntityFireball = true;
                        clearClassPoolIfNeeded();
                        return newClass;
                    }
                } catch (Exception e) {
                    Exception wrapper = new Exception("Unhandled exception in agent", e);
                    wrapper.printStackTrace();
                }

                return null;
            }
        });
    }

    // Clears the class pool when everything has been done for performance
    private static void clearClassPoolIfNeeded() {
        if (hasInjectedIntoExplosionDamageCalculator && hasInjectedIntoEntityFireball) {
            classPool = null;
        }
    }

    private static byte[] redefineExplosionDamageCalculator(String internalClassName) throws IOException, CannotCompileException {
        // Class names are internally stored using forward slashes as package separators instead of periods, convert
        // to the form used in the Java language
        String className = internalClassName.replace('/', '.');

        // Fetch a handle to the class being modified
        CtClass cls = classPool.getOrNull(className);

        try {
            // Iterate over the methods in the class
            for (CtBehavior behavior : cls.getDeclaredBehaviors()) {
                // Find the method which returns an Optional
                if (!behavior.getSignature().endsWith("Ljava/util/Optional;")) {
                    continue;
                }

                // Extract the NMS package name
                String nmsPackage = className.substring(0, className.indexOf("ExplosionDamageCalculator") - 1);
                // Replace occurrences of $NMS with the NMS package name in the payload
                String payload = EXPLOSION_DAMAGE_CALCULATOR_INJECTION.replaceAll("\\$NMS", nmsPackage);

                // Add the payload before the other code in the method, without replacing it
                behavior.insertBefore(payload);
            }

            // Return the modified bytecode
            return cls.toBytecode();
        } finally {
            cls.detach();
        }
    }

    private static byte[] redefineFireball(String internalClassName) throws IOException, CannotCompileException {
        // Class names are internally stored using forward slashes as package separators instead of periods, convert
        // to the form used in the Java language
        String className = internalClassName.replace('/', '.');

        // Fetch a handle to the class being modified
        CtClass cls = classPool.getOrNull(className);

        try {
            // Extract the NMS package name
            String nmsPackage = className.substring(0, className.indexOf("EntityFireball") - 1);
            // Replace occurrences of $NMS with the NMS package name in the payload
            CtMethod method = CtNewMethod.make(ENTITY_FIREBALL_INJECTION.replaceAll("\\$NMS", nmsPackage), cls);

            // Inject the payload as a new method
            cls.addMethod(method);
            // Return the modified bytecode
            return cls.toBytecode();
        } finally {
            cls.detach();
        }
    }
}
